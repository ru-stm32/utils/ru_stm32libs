# ru_stm32libs

Libraries for registy level programing stm32 MCU's

## Adding submodule to project:
* add submodule
```bash
mkdir -p libs && cd libs
git submodule add git@gitlab.com:stm32ru/ru_stm32libs.git
```

* add include patch to *Makefile*, example:
```Makefile
INCDIRS += ./libs/ru_stm32libs/f4
```
* add sources patch to *Makefile*, example:
```Makefile
# ru_stm32libs
SRC += \
	./libs/ru_stm32libs/f4/gpio.c \
	./libs/ru_stm32libs/f4/usart.c \
```
