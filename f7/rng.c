#include "stm32f7xx.h"
#include "rng.h"
#include "rcc.h"

void rng_init(void) {
	rcc_AHB2ENR_set(RCC_AHB2ENR_RNGEN);
	RNG->CR |= RNG_CR_RNGEN;
}

uint32_t rng_getRandom(void) {
	uint32_t random = 0;
	while (!(RNG->SR & RNG_SR_DRDY))
		;
	random = (uint32_t) RNG->DR;
	return random;
}
