#include "stm32f7xx.h"
#include "rcc.h"

void rcc_usePLL216Mhz(void){
	FLASH->ACR = FLASH_ACR_ARTRST | 9;                        // reset caches
	FLASH->ACR = FLASH_ACR_ARTEN  |   FLASH_ACR_PRFTEN | 9;	  // enable caches and prefetch

	RCC->CR |=  RCC_CR_HSEON;                       // start HSE
	while(!(RCC->CR & RCC_CR_HSERDY));              // wait for start HSE

	//PLL (25 MHz / 25) * 432) / 2 = 216
	RCC->PLLCFGR = (RCC_PLLCFGR_PLLM  & 25)           // PLLM=25 (crystal 25MHz)
	             | (RCC_PLLCFGR_PLLN  & (432 << 6))   // PLLN=432
	             | (RCC_PLLCFGR_PLLP  & (0<< 16))     // PLLP=0
	             | (RCC_PLLCFGR_PLLQ  & (9<<24))      // PLLQ=9
	             | (RCC_PLLCFGR_PLLSRC_HSE);          // HSE oscillator clock selected as PLL clock entry

	RCC->CR |=  RCC_CR_PLLON;                       // start main PLL
	while(!(RCC->CR & RCC_CR_PLLRDY));              // wait for start main PLL

	RCC->CFGR = RCC_CFGR_PPRE1_DIV4                 // APB1 prescaler /4
	          | RCC_CFGR_PPRE2_DIV2;                // APB2 prescaler /2
	RCC->CFGR |= RCC_CFGR_SW_PLL;                    //10: PLL selected as system clock
}
