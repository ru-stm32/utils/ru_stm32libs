#include "stm32f7xx.h"
#include "usart.h"
#include "stddef.h"
#include "stdarg.h"
#include "stdio.h"

static const uint32_t f_cpu = 16000000;
static usartInstance_t *mUSART1, *mUSART2, *mUSART6;

void usart_init(usartInstance_t *usartInstance) {
	usartInstance->USARTx->BRR = f_cpu / usartInstance->baud;
	usartInstance->USARTx->CR1 |=
	USART_CR1_TE | USART_CR1_RE | USART_CR1_RXNEIE;
	usartInstance->USARTx->CR1 |= USART_CR1_UE;

	if (usartInstance->USARTx == USART1) {
		mUSART1 = usartInstance;
		NVIC_EnableIRQ(USART1_IRQn);
	} else if (usartInstance->USARTx == USART2) {
		mUSART2 = usartInstance;
		NVIC_EnableIRQ(USART2_IRQn);
	} else if (usartInstance->USARTx == USART6) {
		mUSART6 = usartInstance;
		NVIC_EnableIRQ(USART6_IRQn);
	}
}

void usart_printf(usartInstance_t *usartInstance, char *format, ...) {
	va_list args;

	va_start(args, format);
	char *tx = (char *) usartInstance->tx_buffer;
	vsnprintf(tx, TX_BUFFER_SIZE, format, args);

	usartInstance->tx_index = 0;
	usartInstance->tx_isBusy = true;

	if (usartInstance->tx_buffer[usartInstance->tx_index] != 0x00)
		usartInstance->USARTx->TDR = usartInstance->tx_buffer[usartInstance->tx_index];
	usartInstance->USARTx->CR1 |= USART_CR1_TXEIE;
}

static void usart_interruptCallback(usartInstance_t *usartInstance) {
	char data;
	if (usartInstance->USARTx->ISR & USART_ISR_RXNE) {
		usartInstance->USARTx->ISR &= ~ USART_ISR_RXNE;
		data = (uint8_t) usartInstance->USARTx->RDR;

		if (data == 0x0A || data == 0x0D) {
			usartInstance->rx_buffer[usartInstance->rx_index] = 0x00;
			usartInstance->rx_index = 0;
			usartInstance->rx_isReady = true;
		} else {
			usartInstance->rx_buffer[usartInstance->rx_index] = data;
			usartInstance->rx_index++;
		}

	} else if (usartInstance->USARTx->ISR & USART_ISR_TXE) {
		usartInstance->tx_index++;
		if (usartInstance->tx_buffer[usartInstance->tx_index] != 0x00)
			usartInstance->USARTx->TDR = usartInstance->tx_buffer[usartInstance->tx_index];
		else {
			usartInstance->USARTx->CR1 &= ~USART_CR1_TXEIE;
			usartInstance->tx_isBusy = false;
		}
	}
}
///////////////////////////////////////////////////////////////////////////////////
//INTERRUPTS
///////////////////////////////////////////////////////////////////////////////////
void USART1_IRQHandler(void) {
	usart_interruptCallback(mUSART1);
}

void USART2_IRQHandler(void) {
	usart_interruptCallback(mUSART2);
}

void USART6_IRQHandler(void) {
	usart_interruptCallback(mUSART6);
}
