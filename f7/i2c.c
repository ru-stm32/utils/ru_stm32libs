#include "stm32f7xx.h"
#include "i2c.h"
#include "stdbool.h"

////for i2c 100kHz ond cpu clock 8MHz
//#define SCLL 	0x13
//#define SCLH	0xF
//#define SDADEL	0x2
//#define SCLDEL	0x4
//#define PRESC	1

//for i2c 400kHz ond cpu clock 8MHz
#define SCLL 	0x09
#define SCLH	0x03
#define SDADEL	0x01
#define SCLDEL	0x03
#define PRESC	0

static i2cInstance_t *mI2C1, *mI2C2, *mI2C3;

void i2c_init(i2cInstance_t *i2cInstance) {
	i2cInstance->I2Cx->CR1 &= ~I2C_CR1_PE;
	i2cInstance->I2Cx->CR1 |= I2C_CR1_TXIE | I2C_CR1_TCIE;
	i2cInstance->I2Cx->TIMINGR = SCLL | (SCLH << 8) | (SDADEL << 16) | (SCLDEL << 20) | (PRESC << 28);
//	I2Cx->TIMINGR = 0x00303D5B;
	i2cInstance->I2Cx->CR1 |= I2C_CR1_PE;

	if (i2cInstance->I2Cx == I2C1) {
		mI2C1 = i2cInstance;
		NVIC_EnableIRQ(I2C1_EV_IRQn);
	} else if (i2cInstance->I2Cx == I2C2) {
		mI2C2 = i2cInstance;
		NVIC_EnableIRQ(I2C2_EV_IRQn);
	} else if (i2cInstance->I2Cx == I2C3) {
		mI2C3 = i2cInstance;
		NVIC_EnableIRQ(I2C3_EV_IRQn);
	}
}

bool i2c_sendData(i2cInstance_t *i2cInstance) {
//	if ((i2cInstance->I2Cx->ISR & I2C_ISR_BUSY))
//		return false;

	i2cInstance->I2Cx->CR2 = (i2cInstance->slaveAddress) | (i2cInstance->dataSize << 16) | I2C_CR2_AUTOEND;
	i2cInstance->I2Cx->CR2 |= I2C_CR2_START;
	i2cInstance->busBusy = true;

	return true;
}

static void i2c_interruptCallback(i2cInstance_t *i2cInstance) {
	static uint8_t index = 0;
	if (i2cInstance->I2Cx->ISR & I2C_ISR_TXIS) {
		i2cInstance->I2Cx->ISR &= ~I2C_ISR_TXIS;
		i2cInstance->I2Cx->TXDR = i2cInstance->dataBuffor[index];
		index++;

		if (index == (i2cInstance->dataSize)) {
			index = 0;
			i2cInstance->busBusy = false;
		}

	}
//	else if (i2cInstance->I2Cx->ISR & I2C_ISR_TC) {
//		i2cInstance->I2Cx->ISR &= ~I2C_ISR_TC;
//		i2cInstance->I2Cx->CR2 |= I2C_CR2_STOP;
//		index = 0;
//	}
}

void I2C1_EV_IRQHandler(void) {
	i2c_interruptCallback(mI2C1);
}

void I2C2_EV_IRQHandler(void) {
	i2c_interruptCallback(mI2C2);
}

void I2C3_EV_IRQHandler(void) {
	i2c_interruptCallback(mI2C3);
}
