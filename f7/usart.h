#ifndef RU_STM32LIBS_F7_USART_H_
#define RU_STM32LIBS_F7_USART_H_

#define TX_BUFFER_SIZE 32
#define RX_BUFFER_SIZE 32
#include "stdbool.h"

typedef volatile struct {
	USART_TypeDef *USARTx;
	uint32_t baud;
	char tx_buffer[TX_BUFFER_SIZE];
	uint8_t tx_index;
	bool tx_isBusy;
	char rx_buffer[RX_BUFFER_SIZE];
	uint8_t rx_index;
	bool rx_isReady;
} usartInstance_t;

void usart_init(usartInstance_t *usartInstance);
void usart_printf(usartInstance_t *usartInstance, char *format, ...);

#endif /* RU_STM32LIBS_F7_USART_H_ */
