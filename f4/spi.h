#ifndef RU_STM32LIBS_F4_SPI_H_
#define RU_STM32LIBS_F4_SPI_H_

#include "stm32f4xx.h"
#include "stdbool.h"

typedef volatile struct{
	SPI_TypeDef *SPIx;
	GPIO_TypeDef *CS_GPIO;
	uint32_t CS_pin;
}spiInstance_t;

void spi_slaveSelect(spiInstance_t *spiInstance);
void spi_slaveDeselect(spiInstance_t *spiInstance);
void spi_init(spiInstance_t *spiInstance);
void spi_transmition(spiInstance_t *spiInstance, uint32_t *dataTx, uint32_t *dataRx, uint32_t lenght);

#endif /* RU_STM32LIBS_F4_SPI_H_ */
