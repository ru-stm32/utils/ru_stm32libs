#ifndef RU_STM32LIBS_F4_I2C_H_
#define RU_STM32LIBS_F4_I2C_H_

#include "stm32f4xx.h"
#include "stdbool.h"

typedef volatile struct{
	I2C_TypeDef *I2Cx;
	uint8_t slaveAddress;
	uint8_t *dataBuffer;
	uint8_t dataSize;
	bool busBusy;
}i2cInstance_t;

void i2c_init(i2cInstance_t *i2cInstance);
bool i2c_sendData(i2cInstance_t *i2cInstance);

#endif /* RU_STM32LIBS_F4_I2C_H_ */
