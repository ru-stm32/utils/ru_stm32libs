/*
 * rcc.h
 *
 *  Created on: Oct 23, 2018
 *      Author: rafau
 */

#ifndef RU_STM32LIBS_F4_RCC_H_
#define RU_STM32LIBS_F4_RCC_H_

#include "stm32f4xx.h"
#include "stdbool.h"

static inline void rcc_APB1ENR_set(uint32_t b) {
	RCC->APB1ENR |= b;
}

static inline void rcc_APB1ENR_clear(uint32_t b) {
	RCC->APB1ENR &= ~b;
}

static inline void rcc_APB2ENR_set(uint32_t b) {
	RCC->APB2ENR |= b;
}

static inline void rcc_APB2ENR_clear(uint32_t b) {
	RCC->APB2ENR &= ~b;
}

static inline void rcc_AHB1ENR_set(uint32_t b) {
	RCC->AHB1ENR |= b;
}

static inline void rcc_AHB1ENR_clear(uint32_t b) {
	RCC->AHB1ENR &= ~b;
}

uint32_t rcc_get_SYSCLK(void);

#endif /* RU_STM32LIBS_F4_RCC_H_ */
