/*
 * rcc.c
 *
 *  Created on: Oct 23, 2018
 *      Author: rafau
 */
#include "rcc.h"

uint32_t rcc_get_SYSCLK(void) {
	if ((RCC->CFGR & (RCC_CFGR_SWS_0 | RCC_CFGR_SWS_0)) == RCC_CFGR_SWS_HSI)
		return 16000000UL;
	else if ((RCC->CFGR & (RCC_CFGR_SWS_0 | RCC_CFGR_SWS_0)) == RCC_CFGR_SWS_HSE)
		return 0;	//TODO
	else if ((RCC->CFGR & (RCC_CFGR_SWS_0 | RCC_CFGR_SWS_0)) == RCC_CFGR_SWS_PLL)
		return 0;	//TODO

	return 0;
}
