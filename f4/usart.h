#ifndef RU_STM32LIBS_F4_USART_H_
#define RU_STM32LIBS_F4_USART_H_

#include "stm32f4xx.h"

#include "stddef.h"

void usart_init_8d1s_np(USART_TypeDef *USARTx, uint32_t baud, int16_t irq_e);
void usart_write_blocking(USART_TypeDef *USARTx, char *data, size_t size);

#endif /* RU_STM32LIBS_F4_USART_H_ */
