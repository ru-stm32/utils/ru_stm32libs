#include "stm32f4xx.h"
#include "stdbool.h"
#include "spi.h"

void inline spi_slaveSelect(spiInstance_t *spiInstance) {
	spiInstance->CS_GPIO->BSRR = (1 << (16 + spiInstance->CS_pin));
}

void inline spi_slaveDeselect(spiInstance_t *spiInstance) {
	spiInstance->CS_GPIO->BSRR = (1 << (spiInstance->CS_pin));
}

void spi_init(spiInstance_t *spiInstance) {
	spiInstance->SPIx->CR1 = (0x07 << 3) | SPI_CR1_SSI | SPI_CR1_SSM | SPI_CR1_MSTR | SPI_CR1_SPE;

	if (spiInstance->CS_pin > 15) spiInstance->CS_pin = 15;

	spi_slaveDeselect(spiInstance);
}

void spi_transmition(spiInstance_t *spiInstance, uint32_t *dataTx, uint32_t *dataRx, uint32_t lenght) {
	uint32_t index = 0;
	while (!(spiInstance->SPIx->SR & SPI_SR_TXE))
		;
	spiInstance->SPIx->DR = dataTx[index++];
	while (!(spiInstance->SPIx->SR & SPI_SR_RXNE))
		;
	dataRx[0] = spiInstance->SPIx->DR;

	for (; index < lenght; ++index) {

		spiInstance->SPIx->DR = dataTx[index];
		while (!(spiInstance->SPIx->SR & SPI_SR_RXNE))
			;
		dataRx[index - 1] = spiInstance->SPIx->DR;
	}

	spiInstance->SPIx->DR = 0;

	while (!(spiInstance->SPIx->SR & SPI_SR_RXNE))
		;

	dataRx[index-1] = spiInstance->SPIx->DR;
}
