#include "stm32f4xx.h"

#include "stdarg.h"
#include "stdbool.h"
#include "stddef.h"
#include "stdio.h"
#include "string.h"

#include "rcc.h"
#include "usart.h"

void usart_init_8d1s_np(USART_TypeDef *USARTx, uint32_t baud, int16_t irq_e) {
    USARTx->BRR = rcc_get_SYSCLK() / baud;
    USARTx->CR1 |= USART_CR1_TE | USART_CR1_RE | irq_e;
    USARTx->CR1 |= USART_CR1_UE;
}

void usart_write_blocking(USART_TypeDef *USARTx, char *data, size_t size) {
    for (size_t i = 0; i < size; i++) {
        while (!(USARTx->SR & USART_SR_TXE))
            ;
        USARTx->DR = data[i];
    }
    while (!(USARTx->SR & USART_SR_TC))
        ;
}
